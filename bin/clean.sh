#!/bin/bash
rm tmp/cache/models/*
rm tmp/cache/persistent/*
rm tmp/cache/views/*
rm tmp/sessions/*
rm tmp/tests/*