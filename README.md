# CakePHP 4
Learn CakePHP 4.

## Pre-requisitions
- Install PHP, MySQL, and Apache (or nginx as your preference).
- mbstring PHP extension
- intl PHP extension
- SimpleXML PHP extension
- PDO PHP extension
- Install [composer](https://getcomposer.org/download/#main).

```bash
$ sudo apt upadte
$ sudo apt install apache2
$ sudo apt install php php-mbstring php-intl php-xml
$ sudo apt install mysql-server
```

Allow root user login MySql
```bash
$ sudo mysql
mysql > ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';
mysql > FLUSH PRIVILEGES;
mysql > exit
```

## First Time
```bash
$ composer create-project --prefer-dist cakephp/app:4.* cms
```

Create database in Mysql

```bash
$ cd cms
$ mysql -u root -p
$ <enter your password>
mysql> create database cms;
mysql> source config/schema/schema.sql;
mysql> create database debugkit;
mysql> exit
```

```bash
$ cd cms
$ composer require "cakephp/authentication:^2.0"
$ composer require "cakephp/authorization:^2.0"
$ bin/cake bake all articles
$ bin/cake bake all tags
$ bin/cake bake all users
$ bin/cake server
```

Open http://localhost:8765 in browser

## Gettings Started
Create database manually if not exist.

```bash
$ git clone https://gitlab.com/yancyn/cms.git
$ cd cms
$ composer install
$ bin/cake server
```

## Testing
First time.

```bash
$ mysql -u root -p
mysql> <enter your password>
mysql> create database test_cake_cms;
mysql> source tests/schema.sql;
mysql> exit
```

- Execute all unit tests `$ vendor/bin/phpunit`
- Execute per file `$ vendor/bin/phpunit tests/TestCase/Controller/ArticlesControllerTest.php`

## References
1. https://book.cakephp.org/4/en/tutorials-and-examples.html